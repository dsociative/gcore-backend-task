package mapi

import (
	"encoding/json"
	"net/http"
)

type VHost struct {
	Name   string
	Origin string
	Port   int
}

type AgentConfig struct {
	VHosts map[string]VHost
}

type Fetcher struct {
	url string
}

func NewFetcher(url string) Fetcher {
	return Fetcher{url: url}
}

func (f Fetcher) Get() (c AgentConfig, err error) {
	var resp *http.Response
	resp, err = http.Get(f.url)
	if err == nil {
		err = json.NewDecoder(resp.Body).Decode(&c)
	}
	return c, err
}
