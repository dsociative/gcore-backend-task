package mapi

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFetcher(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, err := ioutil.ReadFile("test_data/agent_config.json")
		if err != nil {
			log.Fatal(err)
		}
		w.Write(b)
	}))
	defer ts.Close()
	f := NewFetcher(ts.URL)
	c, err := f.Get()
	assert.NoError(t, err)
	assert.Equal(t, AgentConfig(AgentConfig{VHosts: map[string]VHost{"e.rewr.ru": {Name: "e.rewr.ru", Origin: "9gag.com"}}}), c)
}
