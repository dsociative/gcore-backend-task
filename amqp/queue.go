package amqp

import (
	"bitbucket.org/dsociative/gcore-backend-task/mapi"
	"encoding/json"
	"errors"
	"github.com/streadway/amqp"
	"log"
	"time"
)

var (
	amqConnectionErr = errors.New("AMQ not connected")
	queues           = []string{"agent_config"}
)

type AMQ struct {
	addr        string
	conn        *amqp.Connection
	ch          *amqp.Channel
	connectFunc func(addr string) (*amqp.Connection, *amqp.Channel, error)
}

func NewAMQ(addr string) *AMQ {
	return &AMQ{
		addr:        addr,
		connectFunc: connect,
	}
}

func connect(addr string) (conn *amqp.Connection, ch *amqp.Channel, err error) {
	conn, err = amqp.Dial(addr)
	if err == nil {
		ch, err = conn.Channel()
		if err == nil {
			err = ch.Qos(1, 0, false)
			if err == nil {
				for _, queueName := range queues {
					_, err = ch.QueueDeclare(
						queueName, // name
						true,      // durable
						false,     // delete when unused
						false,     // exclusive
						false,     // no-wait
						nil,       // arguments
					)
					if err != nil {
						return conn, ch, err
					}
				}
			}
		}
	}
	return conn, ch, err
}

func (q *AMQ) Connect() (err error) {
	q.conn, q.ch, err = q.connectFunc(q.addr)
	return err
}

func (q *AMQ) Close() {
	if q.conn != nil {
		q.conn.Close()
	}
	if q.ch != nil {
		q.ch.Close()
	}
}
func (q *AMQ) Watch(c chan *amqp.Error) {
	c = q.ch.NotifyClose(c)
	for e := range c {
		log.Println(e)
		q.ch = nil
		break
	}
	q.TryConnectAndWatch()
}

func (q *AMQ) TryConnectAndWatch() (err error) {
	for {
		err = q.Connect()
		if err == nil {
			go q.Watch(make(chan *amqp.Error))
			return err
		} else {
			time.Sleep(time.Second)
		}
	}
	return err
}

func (q *AMQ) Send(query string, source mapi.AgentConfig) (err error) {
	var b []byte
	b, err = json.Marshal(&source)
	if err == nil {
		err = q.SendBytes(query, b)
	}
	return err
}

func (q *AMQ) SendBytes(query string, b []byte) (err error) {
	if q.ch == nil {
		return amqConnectionErr
	} else {
		return q.ch.Publish(
			"",    // exchange
			query, // routing key
			false, // mandatory
			false,
			amqp.Publishing{
				DeliveryMode: amqp.Transient,
				ContentType:  "application/json",
				Body:         b,
			})
	}
}

func (q *AMQ) Consume(query string) (<-chan amqp.Delivery, error) {
	return q.ch.Consume(
		query, // queue
		"",    // consumer
		false, // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
}
