package main

import (
	"bitbucket.org/dsociative/gcore-backend-task/amqp"
	"bitbucket.org/dsociative/gcore-backend-task/mapi"
	"flag"
	"log"
)

var (
	amqpAddr   = flag.String("amqp_addr", "amqp://localhost:5672/", "amqp addr")
	sourseAddr = flag.String("source_addr", "http://gcore-api-branch-1.apidev.spcdn.ru/agent_config", "sorce addr")
)

func main() {
	flag.Parse()
	a := amqp.NewAMQ(*amqpAddr)
	f := mapi.NewFetcher(*sourseAddr)

	err := a.Connect()
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := f.Get()
	if err != nil {
		log.Fatal(err)
	}

	err = a.Send("agent_config", cfg)
	if err != nil {
		log.Fatal(err)
	}
}
