package main

import (
	"bitbucket.org/dsociative/gcore-backend-task/agent"
	"bitbucket.org/dsociative/gcore-backend-task/amqp"
	"bitbucket.org/dsociative/gcore-backend-task/mapi"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	amqpAddr = flag.String("amqp_addr", "amqp://localhost:5672/", "amqp addr")
)

func main() {
	flag.Parse()
	a := amqp.NewAMQ(*amqpAddr)
	err := a.Connect()
	if err != nil {
		log.Fatal(err)
	}

	msg, err := a.Consume("agent_config")
	for m := range msg {
		cfg := mapi.AgentConfig{}
		err := json.Unmarshal(m.Body, &cfg)
		if err != nil {
			fmt.Fprintln(os.Stderr, "anget_config parse err: "+err.Error())
		} else {
			rendered, err := agent.Render(cfg)
			if err != nil {
				fmt.Fprintln(os.Stderr, "anget_config render err: "+err.Error())
			} else {
				fmt.Println(rendered)
				m.Ack(true)
			}
		}

	}
}
