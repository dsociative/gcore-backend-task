package agent

import (
	"bitbucket.org/dsociative/gcore-backend-task/mapi"
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRenderVhost(t *testing.T) {
	b := bytes.Buffer{}
	vhostTemplate.Execute(
		&b,
		mapi.VHost{
			Name:   "name_test",
			Origin: "domain.com",
			Port:   8080,
		},
	)
	assert.Equal(
		t,
		`upstream name_test {
  server domain.com max_fails=0;
}

server {
    listen 8080;
    server_name name_test ;

    access_log /var/log/nginx/name_test_access.log;
    error_log  /var/log/nginx/name_test_error.log;

    location / {
        proxy_pass http://name_test;
    }
}
`,
		b.String(),
	)
}

func TestRenderNginx(t *testing.T) {
	b := bytes.Buffer{}
	nginxTemplate.Execute(
		&b,
		NginxConf{Configs: `upstream name_test {
  server domain.com max_fails=0;
}

server {
    listen 8080;
    server_name name_test ;

    access_log /var/log/nginx/name_test_access.log;
    error_log  /var/log/nginx/name_test_error.log;

    location / {
        proxy_pass http://name_test;
    }
}`},
	)
	assert.Equal(
		t,
		`error_log  /tmp/error.log;
pid        /tmp/nginx.pid;

events {
    worker_connections  65536;
}

http {
    access_log /tmp/access.log;
    client_header_timeout 10s;
    client_body_timeout   10s;
    default_type  application/octet-stream;
    sendfile    on;

upstream name_test {
  server domain.com max_fails=0;
}

server {
    listen 8080;
    server_name name_test ;

    access_log /var/log/nginx/name_test_access.log;
    error_log  /var/log/nginx/name_test_error.log;

    location / {
        proxy_pass http://name_test;
    }
}

}
`,
		b.String(),
	)
}

func TestRender(t *testing.T) {
	r, err := Render(
		mapi.AgentConfig{
			VHosts: map[string]mapi.VHost{
				"name_test": {
					Name:   "name_test",
					Origin: "domain.com",
					Port:   8080,
				},
				"else_test": {
					Name:   "else_test",
					Origin: "else.com",
					Port:   8080,
				},
			},
		},
	)
	assert.NoError(t, err)
	assert.Equal(
		t,
		`error_log  /tmp/error.log;
pid        /tmp/nginx.pid;

events {
    worker_connections  65536;
}

http {
    access_log /tmp/access.log;
    client_header_timeout 10s;
    client_body_timeout   10s;
    default_type  application/octet-stream;
    sendfile    on;

upstream name_test {
  server domain.com max_fails=0;
}

server {
    listen 8080;
    server_name name_test ;

    access_log /var/log/nginx/name_test_access.log;
    error_log  /var/log/nginx/name_test_error.log;

    location / {
        proxy_pass http://name_test;
    }
}
upstream else_test {
  server else.com max_fails=0;
}

server {
    listen 8080;
    server_name else_test ;

    access_log /var/log/nginx/else_test_access.log;
    error_log  /var/log/nginx/else_test_error.log;

    location / {
        proxy_pass http://else_test;
    }
}


}
`,
		r,
	)
}
