package agent

import (
	"bitbucket.org/dsociative/gcore-backend-task/mapi"
	"bytes"
)

func Render(c mapi.AgentConfig) (s string, err error) {
	b := bytes.Buffer{}
	for _, vhost := range c.VHosts {
		err = vhostTemplate.Execute(&b, vhost)
		if err != nil {
			return "", err
		}
	}
	return renderNginx(b.String())
}

type NginxConf struct {
	Configs string
}

func renderNginx(configs string) (string, error) {
	b := bytes.Buffer{}
	err := nginxTemplate.Execute(&b, NginxConf{Configs: configs})
	return b.String(), err
}
